<p align="center">
  <img src="https://upload.wikimedia.org/wikipedia/commons/2/24/Ansible_logo.svg" />
</p>

# My Ansible Playbook

**jAnsiblePlaybook** is a repo for holding the collection of ansible playbooks for configuring the Linux machine for different project that I am working for.

# How to use the playbook

    sudo apt-add-repository ppa:ansible/ansible
    sudo apt-get update
    sudo apt-get install ansible sshpass

    ansible-galaxy install -r requirements.yml
    ansible-playbook \<name of the playbook you want to execute\>

    example: ansible-playbook -i inventory.ini tasks/nos_local_dev.yml --ask-pass --ask-become-pass

